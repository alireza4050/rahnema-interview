import { Component, OnInit, HostBinding } from '@angular/core';
import { ProjectsService } from 'src/services/projects';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './template.html',
})
export class ProjectsComponent implements OnInit {
  constructor(
    private projectsService: ProjectsService,
    private route: ActivatedRoute,
  ) {}
  @HostBinding('class.w-full') w100 = true;

  projects;
  displayedColumns = [
    'id',
    'project',
    'owner',
    'company',
    'lang',
    'createdAt',
    'remove',
  ];

  ngOnInit() {
    this.route.data.subscribe(({ projects }) => {
      this.projects = projects;
    });
  }

  remove(id: number) {
    return this.projectsService.remove(id);
  }

  async reset() {
    await this.projectsService.reset();
    this.projects = await this.projectsService.all();
  }
}
