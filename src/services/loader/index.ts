import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  show() {
    (window as any).loader.show();
  }
  hide() {
    (window as any).loader.hide();
  }
}
