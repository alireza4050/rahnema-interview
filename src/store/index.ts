import { StoreModule as Store } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';

export const StoreModule = Store.forRoot(reducers, {
  metaReducers,
  runtimeChecks: {
    strictStateImmutability: true,
    strictActionImmutability: true,
  },
});
