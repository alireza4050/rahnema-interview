import { Component, OnInit } from '@angular/core';
import { CdkStepHeader } from '@angular/cdk/stepper';

@Component({
  selector: 'app-wizard-header',
  templateUrl: './template.html',
})
export class WizardHeaderComponent extends CdkStepHeader implements OnInit {
  ngOnInit() {}
}
