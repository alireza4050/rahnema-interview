module.exports = {
  theme: {
    extend: {
      minHeight: {
        '108': '27rem',
      },
      minWidth: {
        '160': '40rem',
      },
    },
  },
  variants: {},
  plugins: [],
};
