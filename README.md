# Project Maker

An Angular 8 project I created to learn Angular.
Currently uses a mix of Angular Material components and tailwindcss.

## Todos

- [ ] Use Ngrx for data flow
- [ ] Replace material components with cdk extended components and using pure tailwindcss
- [ ] Add tests
- [ ] Code splitting, Lazy Loading and other size optimizations

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `npm run analyze` to show webpack bundle stats after you've built the project using `npm run build`

## Build

Run `npm run build` to build the project in production mode and output stats. The build artifacts will be stored in the `dist/` directory.
