import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class ProjectsService implements Resolve<any> {
  resolve() {
    return this.all();
  }
  constructor(private http: HttpClient) {}

  all() {
    return this.http.get('/api/projects').toPromise();
  }

  create(project) {
    const { projectDetails, projectSettings, deliveryDetails } = project;
    project = {
      ...projectDetails,
      ...projectSettings,
      ...deliveryDetails,
      lang: 'fa',
    };
    return this.http.post('/api/project', project).toPromise();
  }

  remove(id: number) {
    return this.http.delete(`/api/project/${id}`);
  }

  reset() {
    return this.http.delete('/api/projects').toPromise();
  }
}
