import { NgxIndexedDBService } from 'ngx-indexed-db';

const defaultProjects = [
  {
    projectName: 'Apollo',
    projectOwner: 'Me',
    customerName: 'Rahnema',
    lang: 'fa',
    createdAt: new Date(),
  },
];

export async function insertDefaultsIfEmpty(db: NgxIndexedDBService) {
  if ((await db.count()) !== 0) {
    return;
  }
  await Promise.all(defaultProjects.map(p => db.add(p)));
  console.log('Database filled with default data');
}
