import { Injectable } from '@angular/core';
import {
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { from } from 'rxjs';
import { insertDefaultsIfEmpty } from './defaultProjects';
import { NgxIndexedDBService } from 'ngx-indexed-db';

@Injectable()
export class HttpMockRequestInterceptor implements HttpInterceptor {
  constructor(private db: NgxIndexedDBService) {
    this.db.currentStore = 'projects';
    insertDefaultsIfEmpty(this.db);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    if (!req.url.startsWith('/api/')) {
      return next.handle(req);
    }
    return from(
      (async ({ method, url, body }) => {
        if (method === 'GET' && url === '/api/projects') {
          return { status: 200, body: await this.db.getAll() };
        } else if (method === 'POST' && url === '/api/project') {
          return {
            status: 201,
            body: await this.db.add({ ...body, createdAt: new Date() }),
          };
        } else if (method === 'DELETE' && url === '/api/projects') {
          return { status: 204, body: await this.db.clear() };
        } else if (method === 'DELETE' && /\/api\/project\/\d+/.test(url)) {
          const id = +url.substring(url.lastIndexOf('/'));
          return { status: 204, body: await this.db.delete(id) };
        } else {
          return { status: 404, body: undefined };
        }
      })(req).then(res => {
        console.log(
          `HTTP request intercept and responded using fake server: ${req.url}`,
        );
        return new HttpResponse(res);
      }),
    );
  }
}
