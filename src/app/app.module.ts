import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { StoreModule } from 'src/store';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpMockRequestInterceptor } from 'src/services/mock-api';
import { AppDbModule } from './app-db.module';
import { NewProjectComponent, ProjectsComponent } from 'src/pages';
import { AppRoutingModule } from './app-routing.module';

// TODO: Make aot build work with page components declared dynamically
@NgModule({
  declarations: [AppComponent, NewProjectComponent, ProjectsComponent],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    AppDbModule,
    HttpClientModule,
    StoreModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpMockRequestInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
