import { NgModule } from '@angular/core';
import { WizardComponent } from '.';
import { WizardHeaderComponent } from './header';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [WizardComponent, WizardHeaderComponent],
  imports: [CdkStepperModule, CommonModule],
  exports: [WizardComponent],
})
export class WizardModule {}
