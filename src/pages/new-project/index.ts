import { Component, ChangeDetectionStrategy } from '@angular/core';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { wizardSteps, formGroups, summary } from './wizardSteps';
import { ProjectsService } from 'src/services/projects';
import { Router } from '@angular/router';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewProjectComponent {
  constructor(
    private router: Router,
    private projectService: ProjectsService,
  ) {}
  steps = wizardSteps;
  currentStep = 0;
  formGroups = formGroups;
  summary: ReturnType<typeof summary> = [];

  onStepChange(event: StepperSelectionEvent) {
    if (event.selectedIndex === this.steps.length) {
      this.summary = summary(this.formGroups.value);
    }
  }

  onSubmit() {
    console.log('called');
    this.projectService
      .create(this.formGroups.value)
      .then(() => this.router.navigate(['projects']));
  }
}
