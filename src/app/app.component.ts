import { Component, OnInit } from '@angular/core';
import {
  Router,
  NavigationStart,
  NavigationEnd,
  RoutesRecognized,
} from '@angular/router';
import { LoaderService } from 'src/services/loader';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  constructor(private router: Router, private loader: LoaderService) {}

  title = '';
  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.loader.show();
      }
      if (event instanceof RoutesRecognized) {
        this.title = event.state.root.firstChild.data.title;
      }
      if (event instanceof NavigationEnd) {
        this.loader.hide();
      }
    });
  }
}
