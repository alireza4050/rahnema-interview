import { Component, OnInit } from '@angular/core';
import { CdkStepper } from '@angular/cdk/stepper';

@Component({
  selector: 'app-wizard',
  templateUrl: './template.html',
  providers: [{ provide: CdkStepper, useExisting: WizardComponent }],
})
export class WizardComponent extends CdkStepper implements OnInit {
  ngOnInit() {}
}
