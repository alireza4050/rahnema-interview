import {
  FormGroup,
  Validators,
  FormBuilder,
  ValidatorFn,
} from '@angular/forms';

interface Field {
  type: 'text' | 'tel' | 'email' | 'checkbox-group' | 'checkbox' | 'select';
  label: string;
  icon?: string;
  required?: boolean;
  formControlName: string;
  initialValue?: any;
  hint?: string;
  items?: Array<{ text: string; value: any }>;
  validators?: ValidatorFn[];
}

interface Step {
  label: string;
  title?: string;
  formGroupName: string;
  formGroup?: FormGroup;
  fields: Field[];
}

export const wizardSteps: Step[] = [
  {
    label: 'Project Details',
    formGroupName: 'projectDetails',
    fields: [
      {
        type: 'text',
        label: 'Project Name',
        required: true,
        formControlName: 'projectName',
        initialValue: '',
      },
      {
        type: 'text',
        label: 'Project Owner',
        formControlName: 'projectOwner',
      },
      {
        type: 'text',
        label: 'Customer Name',
        formControlName: 'customerName',
      },
      {
        type: 'tel',
        label: 'Contact Phone',
        icon: 'phone',
        formControlName: 'customerPhone',
      },
      {
        type: 'email',
        label: 'Email Address',
        icon: 'email',
        formControlName: 'customerEmail',
      },
      {
        type: 'text',
        label: 'Company Site',
        formControlName: 'customerSite',
      },
    ],
  },
  {
    label: 'Project Settings',
    formGroupName: 'projectSettings',
    fields: [
      {
        type: 'email',
        label: 'Email Address',
        icon: 'email',
        formControlName: 'projectEmail',
      },
      {
        type: 'select',
        items: [{ text: 'English', value: 'en' }],
        initialValue: 'en',
        label: 'Language',
        formControlName: 'lang',
      },
      {
        type: 'select',
        items: [{ text: 'Tehran +3:30 UTC', value: 'Asia/Tehran' }],
        initialValue: 'Asia/Tehran',
        label: 'Time Zone',
        formControlName: 'tz',
      },
      {
        type: 'checkbox-group',
        label: 'Communtication',
        items: [
          { text: 'Email', value: 'email' },
          { text: 'SMS', value: 'sms' },
          { text: 'Phone', value: 'phone' },
        ],
        initialValue: [true, true, false],
        formControlName: 'communticationMethods',
      },
    ],
  },
  {
    label: 'Delivery Details',
    title: 'Setup Your Address',
    formGroupName: 'deliveryDetails',
    fields: [
      {
        type: 'text',
        label: 'Address Line 1',
        hint: 'Please enter your Address.',
        formControlName: 'addressLine1',
      },
      {
        type: 'text',
        label: 'Address Line 2',
        hint: 'Please enter your Address.',
        formControlName: 'addressLine2',
        required: false,
      },

      {
        type: 'text',
        label: 'Postcode',
        hint: 'Please enter your Postcode.',
        formControlName: 'postcode',
      },

      {
        type: 'text',
        label: 'City',
        formControlName: 'city',
      },
      {
        type: 'text',
        label: 'State/Province',
        formControlName: 'state',
      },
      {
        type: 'select',
        label: 'Country',
        formControlName: 'country',
        items: [{ text: 'Iran', value: 'ir' }],
        initialValue: 'ir',
      },
    ],
  },
];

function generateFormControlConfigFromField(field: Field) {
  const { required = true, validators = [], initialValue = '' } = field;
  if (['checkbox-group'].includes(field.type)) {
    const controlConfigs = field.items.map(({ value }, i) => [
      !!initialValue[i],
    ]);
    return [field.formControlName, fb.array(controlConfigs)];
  }
  if (required) {
    validators.unshift(Validators.required);
  }
  return [field.formControlName, [initialValue, validators]];
}

function generateFormGroupConfigFromStep(step: Step) {
  const controlConfigs = step.fields.map(generateFormControlConfigFromField);
  step.formGroup = fb.group(Object.fromEntries(controlConfigs));
  return [step.formGroupName, step.formGroup];
}

const fb = new FormBuilder();
export let formGroups: FormGroup;
const groups = wizardSteps.map(generateFormGroupConfigFromStep);
formGroups = fb.group(Object.fromEntries(groups));

export function summary({
  projectDetails,
  projectSettings,
  deliveryDetails,
}: any) {
  return [
    {
      label: 'Project Details',
      lines: [
        `${projectDetails.projectOwner}, ${projectDetails.projectName} Project`,
        `Phone: ${projectDetails.customerPhone}`,
        `Email: ${projectSettings.projectEmail}`,
      ],
    },
    {
      label: 'Delivery Address',
      lines: [deliveryDetails.addressLine1, deliveryDetails.addressLine2],
    },
  ];
}
