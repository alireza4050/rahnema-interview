import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewProjectComponent, ProjectsComponent } from 'src/pages';
import { ProjectsService } from 'src/services/projects';

const routes: Routes = [
  { path: '', redirectTo: '/new-project', pathMatch: 'full' },
  {
    path: 'new-project',
    component: NewProjectComponent,
    data: { title: 'New Project Wizard' },
  },
  {
    path: 'projects',
    component: ProjectsComponent,
    data: { title: 'Current Projects' },
    resolve: {
      projects: ProjectsService,
    },
  },
];

// export const routeComponents = routes.map(r => r.component).filter(c => c);

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
